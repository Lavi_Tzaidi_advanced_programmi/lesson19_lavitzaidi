﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson19
{
    
    public partial class frmMain : Form
    {
        string nameOfUser = "";
        string[] lines;
        public frmMain(string name)
        {
            nameOfUser = name;
            string FileName = name + "BD.txt";
            lines = System.IO.File.ReadAllLines(FileName);
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            bool found = false;
            string toTest = monthCalendar1.SelectionStart.ToString();
            toTest = toTest.Substring(0, 10);
            string[] date = toTest.Split('/');
            if (date[1][0]=='0') // splitting the number to get the correct date
            {
                date[1] = date[1][1].ToString();
            }
            toTest = date[1] + '/' + date[0] + '/' + date[2];
            foreach (string line in lines)
            {
                if (line.Contains(toTest))// testing if file contains birthday
                {
                    string[] person = line.Split(',');//spliting name from date
                    found = true;
                    label1.Text = person[0]+ " Has A BirthDay Today Mazel Tov";
                    break;
                }
            }
            if(found == false)
            {
                label1.Text ="No One Celebrates Today";
            }
            textBox1.Text = monthCalendar1.SelectionStart.ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
