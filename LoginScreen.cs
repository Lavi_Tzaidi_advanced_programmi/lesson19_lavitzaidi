﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lesson19
{
    public partial class LoginScreen : Form
    {
        public LoginScreen()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool flag = false;
            string[] lines = System.IO.File.ReadAllLines("Users.txt");
            string query = "";
            query = txtUser.Text.Trim()+","+ txtPassword.Text.Trim();
            foreach(string line in lines)
            {
                if(line.Equals(query))
                {
                    flag = true;
                    frmMain main = new frmMain(txtUser.Text.Trim());
                    this.Hide();
                    main.Show(); 
                }
                if(!flag)
                {
                    MessageBox.Show("The UserName or Password you entered dosenot exist in the system");
                }
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            txtPassword.PasswordChar = '*';
        }
    }
}
